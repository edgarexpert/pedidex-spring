package com.jhonystein.pedidex.resources;

import com.jhonystein.pedidex.model.Servico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "servicos", 
        collectionResourceRel = "servico")
public interface ServicoResource 
        extends JpaRepository<Servico, Long>{
    
}
