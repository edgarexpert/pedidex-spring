package com.jhonystein.pedidex.resources;

import com.jhonystein.pedidex.model.Usuario;
import com.jhonystein.pedidex.service.UsuarioService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class AuthResource {

    @Autowired
    private UsuarioService service;
    
    @PostMapping("/login")
    public Usuario login(@RequestBody Usuario usuario) {
        return service.login(usuario);
    }
    
    @PostMapping("/register")
    public Usuario register(@RequestBody Usuario usuario) {
        return service.register(usuario);
    }
    
    @GetMapping("/@me")
    public Usuario getMe() {
        return getUsuario();
    }
    
    @GetMapping("/foto")
    public ResponseEntity<String> getFoto() {
        String fotoBase64 = service.getFoto(getUsuario().getId());
        return fotoBase64 != null ? 
                ResponseEntity.ok(fotoBase64) : 
                ResponseEntity.notFound().build();
    }
    
    @PostMapping("/foto")
    public String saveFoto(@RequestParam MultipartFile foto) throws IOException {
        return service.saveFoto(getUsuario().getId(), foto.getBytes());
    }
    
    private Usuario getUsuario() {
        return (Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
    }
}
