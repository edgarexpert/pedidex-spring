package com.jhonystein.pedidex.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "regras")
@SequenceGenerator(name = "regras_seq", sequenceName = "regras_seq", allocationSize = 1)
public class Regra implements GrantedAuthority {
    
    @Id
    @Column(name = "id_regra")
    @GeneratedValue(generator = "regras_seq", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String regra;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegra() {
        return regra;
    }

    public void setRegra(String regra) {
        this.regra = regra;
    }

    @Override
    public String getAuthority() {
        return this.regra;
    }
    
}
