package com.jhonystein.pedidex.repository;

import com.jhonystein.pedidex.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository 
    extends JpaRepository<Cliente, Long>{

    Cliente findByDocumento(String documento);
}
